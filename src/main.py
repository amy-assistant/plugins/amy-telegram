from telethon import TelegramClient, events, types
from telethon.sessions import StringSession
from amy import Plugin, Message, instance

import asyncio

# import os

# os.environ['PRODUCTION'] = '1'
# os.environ['AMY_Q_HOST'] = '159.89.24.207'

API_ID = '122636'
API_HASH = '44ccf5edd77f8ffb748fc958f89abfbb'


def publishMessage(ctx, username, message):
    if isinstance(message.to_id, types.PeerUser):
        channel = message.to_id.user_id if message.out else message.from_id
    else:
        channel = message.to_id.chat_id

    message = Message('telegram')\
        .setUser(username)\
        .setContent(message.message)\
        .setSender(message.from_id if not message.out else None)\
        .setChannel(channel).setDatetime(message.date.isoformat())\
        .setOut(message.out)

    ctx.publishMessage(message)
    return channel


@events.register(events.NewMessage)
async def handler(event):
    client = event.client
    channel = publishMessage(client.ctx, client.username, event.message)
    await client.send_read_acknowledge(await client.get_entity(channel), event.message)


async def send_message(client, message):
    return await client.send_message(await client.get_entity(int(message['channel'])), message['content'])


@instance
class Telegram(TelegramClient):
    def onCreate(self, ctx, username, session):
        self.username = username
        TelegramClient.__init__(self, StringSession(session),
                                API_ID, API_HASH, loop=asyncio.new_event_loop())
        self.loop.run_until_complete(self.connect())
        if not self.isAuthorized():
            self.loop.run_until_complete(self.send_code_request(username))

        self.ctx = ctx

        self.add_event_handler(handler)

        if self.isAuthorized():
            return self.session.save()

    def onAuth(self, ctx, token):
        self.loop.run_until_complete(self.sign_in(self.username, token))
        return self.session.save()

    def isAuthorized(self):
        return self.loop.run_until_complete(self.is_user_authorized())

    def onStart(self, ctx):
        ctx.startThread(self, self.run_until_disconnected)

    def onStop(self, ctx):
        self.disconnect()
        ctx.stopThread(self)

    def onDelete(self, ctx):
        self.disconnect()
        self.loop.close()

    def onSendMessage(self, ctx, message):
        self.loop.create_task(send_message(self, message)
                              ).add_done_callback(lambda fut: publishMessage(self.ctx, self.username, fut.result()))


def main():
    telegram = Plugin('telegram', Telegram)


if __name__ == '__main__':
    main()
